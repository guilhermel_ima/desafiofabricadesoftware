import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DesafioFabrica {
    private WebDriver driver;
    private String url = "https://testpages.herokuapp.com/styled/basic-html-form-test.html";

    @Before
    public void setUp () {
        //open browser
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get(url);

    }

    @After
    public void tearDown () {
        driver.close();
    }

    @Test
    public void caminhoComSucesso() {

        WebElement username = driver.findElement(By.name("username"));
        WebElement password = driver.findElement(By.name("password"));
        WebElement comments = driver.findElement(By.name("comments"));
        WebElement checkbox1 = driver.findElement(By.xpath("//input[@value='cb1']"));
        WebElement checkbox2 = driver.findElement(By.xpath("//input[@value='cb2']"));
        WebElement multipleSelect = driver.findElement(By.name("multipleselect[]"));
        Select Selectmultipleselect = new Select(multipleSelect);
        WebElement dropdown = driver.findElement(By.name("dropdown"));
        Select seletor = new Select(dropdown);
        WebElement radio2 = driver.findElement(By.xpath("//input[@value='rd2']"));
        WebElement buttonSubmmit = driver.findElement(By.xpath("//input[@value='submit']"));

        username.sendKeys("Guilherme");
        password.sendKeys("senha321");
        comments.clear();
        comments.sendKeys("Teste");
        checkbox1.click();
        checkbox2.click();
        Selectmultipleselect.selectByValue("ms4");
        seletor.selectByValue("dd3");
        radio2.click();
        buttonSubmmit.click();

        //Asserts
        Assert.assertEquals("Guilherme",driver.findElement(By.id("_valueusername")).getText());
        Assert.assertEquals("senha321", driver.findElement(By.id("_valuepassword")).getText());
        Assert.assertEquals("Teste", driver.findElement(By.id("_valuecomments")).getText());
        Assert.assertEquals("cb1", driver.findElement(By.id("_valuecheckboxes0")).getText());
        Assert.assertEquals("cb2", driver.findElement(By.id("_valuecheckboxes1")).getText());
        Assert.assertEquals("rd2", driver.findElement(By.id("_valueradioval")).getText());
        Assert.assertEquals("ms4", driver.findElement(By.id("_valuemultipleselect0")).getText());
        Assert.assertEquals("dd3", driver.findElement(By.id("_valuedropdown")).getText());

    }

    @Test
    public void caminhoAlternativo () {

        WebElement comments = driver.findElement(By.name("comments"));
        WebElement buttonSubmmit = driver.findElement(By.xpath("//input[@value='submit']"));

        comments.clear();
        buttonSubmmit.click();

        //Asserts
        Assert.assertEquals("No Value for username", driver.findElement(By.xpath("//p[1]/strong")).getText());
        Assert.assertEquals("No Value for password", driver.findElement(By.xpath("//p[2]/strong")).getText());
        Assert.assertEquals("No Value for comments", driver.findElement(By.xpath("//p[3]/strong")).getText());

    }
}
