## ⚒ Descrição do Projeto
Automação de testes na plataformaa [HTML Form Example](https://testpages.herokuapp.com/styled/basic-html-form-test.html) Desafio proposto pela [Fabrica de Software](https://www.instagram.com/fabricadesoftwareunipe/) durante o Workshop de QA 2021.1

## 📌 Autor/Contato

| [<img src="https://avatars1.githubusercontent.com/u/62215470?s=460&u=c6dc439e77463ced6dd781733712708b5fbdde65&v=4" width=115><br><sub>@Guilhermelima3</sub>](https://github.com/Guilhermelima3) |
| :---: |


- Linkedin  [Guilherme Lima](https://www.linkedin.com/in/guilherme-lima-marinho-242635196)
- E-mail [guilhermel_ima@hotmail.com]
---
